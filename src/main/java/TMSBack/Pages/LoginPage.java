package TMSBack.Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {
    public LoginPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@id='login']")
    private WebElement loginField;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordField;

    @FindBy(xpath = "//button[@type='submit']/..")
    private WebElement loginButton;


    @Step
    public ParticipantPage loginToTMSBack(){
        loginField.sendKeys("Autotest@test.de");
        passwordField.sendKeys("testuser");
        loginButton.click();
        return new ParticipantPage(driver);
    }








}
