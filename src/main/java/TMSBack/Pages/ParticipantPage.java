package TMSBack.Pages;

import TMSFront.Pages.HomePage;
import io.qameta.allure.Step;
import net.bytebuddy.implementation.bytecode.Throw;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ParticipantPage extends BasePage {

    public ParticipantPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }



    @FindBy(xpath = "//li/a[@href = contains(text(),'Dashboard')]")
    public WebElement dashboardTab;

    @FindBy(xpath = "//select[@id='workflowMain']")
    private WebElement workflowChooser;

    @FindBy(xpath = "//a[text()='Einstellungen ändern']")
    private WebElement editingWF;

    @FindBy(xpath = "//i[@class='fa fa-cog']")
    private WebElement optionsWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'Standteam John Deere Vertrieb')]")
    private WebElement firstWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'Standteam John Deere International (2)')]")
    private WebElement secondWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'Vertriebspartner John Deere Vertrieb (3)')]")
    private WebElement thirdWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'Vertriebspartner John Deere International (4)')]")
    private WebElement fourthWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'Zweibrücken Engineering (5)')]")
    private WebElement fifthWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'JDWM (6)')]")
    private WebElement sixthWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'ETIC (7)')]")
    private WebElement seventhWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'SAM/KAM (8)')]")
    private WebElement eighthWF;

    @FindBy(xpath = "//select[@id='workflowMain']/option[contains(text(),'Studenten/Praktikanten/Product Genius (9)')]")
    private WebElement ninthWF;

    @FindBy(xpath = "//td[text()='Eingeladen']/..//i[@class='fa fa-cog']")
    private WebElement invitedBooking;

    @FindBy(xpath = "//td[text()='Bestätigt']/..//i[@class='fa fa-cog']")
    private WebElement approvedBooking;

    @FindBy(xpath = "//td[text()='Storniert']/..//i[@class='fa fa-cog']")
    private WebElement canceledBooking;

    @FindBy(xpath = "//div[@class='custom-dropdown-menu opened-custom-dropdown-menu']/a[@target='_blank']")
    private WebElement loginToTMSFrontButton;

    @FindBy(xpath = "//select[@id='bStatus']")
    private WebElement bookingStatusChooser;

    @FindBy(xpath = "//option[@value='2: invited']")
    private WebElement invitedStatus;

    @FindBy(xpath = "//option[@value='6: canceled']")
    private WebElement canceledStatus;

    @FindBy(xpath = "//option[@value='5: approved']")
    private WebElement approvedStatus;

    @FindBy(xpath = "//button[@class='custom-button blue-background width-190']")
    public WebElement createNewParticipantButton;

    @FindBy(xpath ="//label[@for='tab2']")
    public WebElement createNewParticipantManualTab;

    @FindBy(xpath = "//select[@id='workflowId']")
    public WebElement creationWFChooser;

    @FindBy(xpath = "//option[@value='2: Herr']")
    public WebElement creationSex;

    @FindBy(xpath = "//option[@value='1: Dr.']")
    public WebElement creationTitel;

    @FindBy(xpath = "//input[@name='name']")
    public WebElement creationName;

    @FindBy(xpath = "//input[@name='surname']")
    public WebElement creationSurname;

    @FindBy(xpath = "//input[@name='email']")
    public WebElement creationEmail;

    @FindBy(xpath = "//button[@class='custom-button blue-background px-3'][@_ngcontent-c18]")
    public WebElement creationSaveButton;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'Studenten/Praktikanten/Product Genius (9)')]")
    public WebElement creationNinthWF;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'SAM/KAM (8)')]")
    public WebElement creationEighthWF;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'ETIC (7)')]")
    public WebElement creationSeventhWF;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'JDWM (6)')]")
    public WebElement creationSixthWF;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'Zweibrücken Engineering (5)')]")
    public WebElement creationFifthWF;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'Vertriebspartner John Deere International (4)')]")
    public WebElement creationFourthWF;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'Vertriebspartner John Deere Vertrieb (3)')]")
    public WebElement creationThirdWF;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'Standteam John Deere International (2)')]")
    public WebElement creationSecondWF;

    @FindBy(xpath = "//select[@id='workflowId']/option[contains(text(),'Standteam John Deere Vertrieb')]")
    public WebElement creationFirstWF;


    @Step
    public ParticipantPage chooseTheWF(int numberOfWF){
        switch (numberOfWF){
            case 1: firstWF.click();
                break;
            case 2: secondWF.click();
                break;
            case 3: thirdWF.click();
                break;
            case 4: fourthWF.click();
                break;
            case 5: fifthWF.click();
                break;
            case 6: sixthWF.click();
                break;
            case 7: seventhWF.click();
                break;
            case 8: eighthWF.click();
                break;
            case 9: ninthWF.click();
                break;
        }
        return this;
    }

    @Step
    public ParticipantPage chooseBookingStatus(String status){
        switch (status){
            case "Invited": invitedStatus.click();
                break;
            case "Approved": approvedStatus.click();
                break;
            case "Canceled": canceledStatus.click();
                break;
        }
        return this;
    }

    @Step
    public HomePage loginToBookingWithStatus(String bookingStatus) {
        List list = new ArrayList();
        switch(bookingStatus){
            case "Invited":
                new WebDriverWait(driver,10).until(ExpectedConditions
                            .presenceOfElementLocated(By.xpath("//td[text()='Eingeladen']/..//i[@class='fa fa-cog']")));
                    invitedBooking.click();
                    loginToTMSFrontButton.click();
                ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
                driver.switchTo().window(tabs.get(1));
                new WebDriverWait(driver,10).until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("//img[@alt='logo']")));
                break;
            case "Approved":
                new WebDriverWait(driver,10).until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("//td[text()='Bestätigt']/..//i[@class='fa fa-cog']")));
                    approvedBooking.click();
                    loginToTMSFrontButton.click();
                ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
                driver.switchTo().window(tabs2.get(1));
                new WebDriverWait(driver,10).until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("//img[@alt='logo']")));
                break;
            case "Canceled":
                new WebDriverWait(driver,10).until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("//td[text()='Storniert']/..//i[@class='fa fa-cog']")));
                    canceledBooking.click();
                    loginToTMSFrontButton.click();
                ArrayList<String> tabs3 = new ArrayList<String> (driver.getWindowHandles());
                driver.switchTo().window(tabs3.get(1));
                new WebDriverWait(driver,10).until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("//img[@alt='logo']")));
                break;
        }
        return new HomePage(driver);
    }

    @Step
    public HomePage loginToApprovedBooking(){
        approvedBooking.click();
        loginToTMSFrontButton.click();
        return new HomePage(driver);
    }

    @Step
    public HomePage loginToCanceledBooking(){
        canceledBooking.click();
        loginToTMSFrontButton.click();
        return new HomePage(driver);
    }

    @Step
    public ParticipantPage createABooking(int numberOfBooking){
        createNewParticipantButton.click();
        new WebDriverWait(driver,10).until(ExpectedConditions
                .elementToBeClickable(creationWFChooser));
        creationWFChooser.click();
        switch (numberOfBooking) {
            case 1: creationFirstWF.click();
                break;
            case 2: creationSecondWF.click();
                break;
            case 3: creationThirdWF.click();
                break;
            case 4: creationFourthWF.click();
                break;
            case 5: creationFifthWF.click();
                break;
            case 6: creationSixthWF.click();
                break;
            case 7: creationSeventhWF.click();
                break;
            case 8: creationEighthWF.click();
                break;
            case 9: creationNinthWF.click();
                break;
        }
        createNewParticipantManualTab.click();
        creationSex.click();
        creationTitel.click();
        creationName.sendKeys("test");
        creationSurname.sendKeys("test");
        String email = getRandomString();
        creationEmail.sendKeys(email+"@example.com");
        creationEmail.click();
        creationEmail.sendKeys(Keys.ENTER);
        new WebDriverWait(driver,10).until(ExpectedConditions
                .elementToBeClickable(invitedStatus));
        invitedStatus.click();
        //Need to add creation check
        return this;
    }

    private static String getRandomString(){
        String randomSymbols="";
        char c;
        Random r = new Random();
        for(int i=0;i<8;i++){
            c = (char)(r.nextInt(26) + 'a');
            randomSymbols = randomSymbols + c;
        }
        return randomSymbols;
    }



}
