package TMSBack.Tests;

import TMSBack.Pages.LoginPage;
import TMSBack.Pages.ParticipantPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest{

    @Test
    public void successLoginTest(){
        ParticipantPage participantPage = new LoginPage(driver)
                .loginToTMSBack();
        Assert.assertTrue(participantPage.dashboardTab.isDisplayed(),"|Dashboard tab is displayed. Login was successful|");
    }
}
