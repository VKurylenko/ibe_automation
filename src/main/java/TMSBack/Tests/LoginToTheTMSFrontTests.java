package TMSBack.Tests;

import TMSBack.Pages.LoginPage;
import TMSFront.Pages.HomePage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginToTheTMSFrontTests extends BaseTest {
    @Test
    public void loginToFirstWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(1)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSecondWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(2)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToThirdWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(3)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToFourthWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(4)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToFifthWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(5)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSixthWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(6)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSeventhWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(7)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToEighthWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(8)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToNinthWFInvitedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(9)
                .chooseBookingStatus("Invited")
                .loginToBookingWithStatus("Invited");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToFirstWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(1)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSecondWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(2)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToThirdWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(3)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToFourthWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(4)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToFifthWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(5)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSixthWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(6)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSeventhWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(7)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToEighthWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(8)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToNinthWFApprovedBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(9)
                .chooseBookingStatus("Approved")
                .loginToBookingWithStatus("Approved");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToFirstWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(1)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSecondWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(2)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToThirdWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(3)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToFourthWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(4)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToFifthWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(5)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSixthWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(6)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToSeventhWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(7)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToEighthWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(8)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

    @Test
    public void loginToNinthWFCanceledBooking(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSBack()
                .chooseTheWF(9)
                .chooseBookingStatus("Canceled")
                .loginToBookingWithStatus("Canceled");
        Assert.assertTrue(homePage.logoJD.isDisplayed());
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
    }

}
