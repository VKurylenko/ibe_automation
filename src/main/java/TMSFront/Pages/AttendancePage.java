package TMSFront.Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AttendancePage extends BasePage {
    public AttendancePage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//label[@for='option_0_singleTableService_2_8284']")
    private WebElement firstAttendanceSelector;

    @FindBy(xpath = "//label[@for='option_0_singleTableService_2_8285']")
    private WebElement secondAttendanceSelector;

    @FindBy(xpath = "//label[@for='option_0_singleTableService_13_8287']")
    public WebElement iPadSelector;

    @FindBy(xpath = "//app-button-checkout-navigation/button[2]")
    private WebElement nextStepButton;


    @Step
    public HotelStep runAttendanceStep(){
        firstAttendanceSelector.click();
        secondAttendanceSelector.click();
        iPadSelector.click();
        nextStepButton.click();
        return new HotelStep(driver);
    }



}
