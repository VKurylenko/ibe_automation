package TMSFront.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DataProtectionPage extends BasePage {
    public DataProtectionPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//h1")
    public WebElement dataProtectionTitle;

    @FindBy(xpath = "//img[@alt='logo']")
    public WebElement logoJD;


}
