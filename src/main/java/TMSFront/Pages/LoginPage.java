package TMSFront.Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage {
    public LoginPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@id='email']")
    private WebElement email;

    @FindBy(xpath = "//input[@id='password']")
    private WebElement password;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement loginButton;

    @FindBy(xpath = "//span[@class='box']")
    private WebElement acceptTermsAndConditionsCheckbox;

    @Step
    public HomePage loginToTMSFrontByUsualLogin(){
        email.sendKeys("jd-agritechnica-62@example.com");
        password.sendKeys("lsjkf84730-hf2");
        acceptTermsAndConditionsCheckbox.click();
        loginButton.click();
        return new HomePage(driver);
    }


}
