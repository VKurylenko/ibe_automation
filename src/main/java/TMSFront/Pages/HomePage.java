package TMSFront.Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage {
    public HomePage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//img[@alt='logo']")
    public WebElement logoJD;

    @FindBy(xpath = "//button[@class='button btn']")
    public WebElement registrationButton;

    @FindBy(xpath = "//a[@class='nav-link']")
    private WebElement contactPageButton;

    @FindBy(xpath = "//a[contains(@href,'datenschutz')]/..")
    private WebElement dataProtectionPageButton;

    @FindBy(xpath = "//a[contains(@href,'impressum')]")
    private WebElement imprintPageButton;

    @FindBy(xpath = "//span")
    private WebElement cookiesAccept;


    @Step
    public ImprintPage toImprintPage(){
        cookiesAccept.click();
        imprintPageButton.click();
        return new ImprintPage(driver);
    }

    @Step
    public DataProtectionPage toDataProtectionPage(){
        cookiesAccept.click();
        new WebDriverWait(driver,10)
                .until(ExpectedConditions.numberOfElementsToBe(By.xpath("//span"),0));
        dataProtectionPageButton.click();
        return new DataProtectionPage(driver);
    }

    @Step
    public ContactPage toContactPage(){
        cookiesAccept.click();
        contactPageButton.click();
        return new ContactPage(driver);
    }

    @Step
    public ContactDetailsStep clickRegistrationButton(){
        registrationButton.click();
        return new ContactDetailsStep(driver);
    }


}
