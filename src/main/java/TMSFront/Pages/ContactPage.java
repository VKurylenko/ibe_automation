package TMSFront.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactPage extends BasePage {
    public ContactPage(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath = "//h1")
    public WebElement contactTitle;

    @FindBy(xpath = "//img[@alt='logo']")
    public WebElement logoJD;
}
