package TMSFront.Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Random;

public class ContactDetailsStep extends BasePage {
    public ContactDetailsStep(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//option[@value='1: Frau']")
    private WebElement anrede;

    @FindBy(xpath = "//option[@value='1: Dr.']")
    private WebElement titel;

    @FindBy(xpath = "//input[@id='Vorname']")
    private WebElement vorname;

    @FindBy(xpath = "//input[@id='Nachname']")
    private WebElement nachname;

    @FindBy(xpath = "//input[@id='Einheit']")
    private WebElement einheit;

    @FindBy(xpath = "//input[@id='Hub-Einteilung']")
    private WebElement hubEinteilung;

    @FindBy(xpath = "//input[@id='Kostenstelle']")
    private WebElement  kostenstelle;

    @FindBy(xpath = "//input[@id='Straße, Hausnummer']")
    private WebElement strasseAndHouseNumber;

    @FindBy(xpath = "//input[@id='PLZ']")
    private WebElement plz;

    @FindBy(xpath = "//input[@id='Ort']")
    private WebElement ort;

    @FindBy(xpath = "//input[@id=\"E-Mail-Adresse\"]")
    private static WebElement email;

    @FindBy(xpath = "//option[@value='3: Bosnisch']")
    private WebElement nativeLanguage;

    @FindBy(xpath = "//input[@id='Position im Unternehmen']")
    private WebElement positionAtCompany;

    @FindBy(xpath = "//button[@class='button btn']")
    private WebElement nextStepButton;

    @FindBy(xpath = "//input[@name='country']")
    private WebElement language;

    @FindBy(xpath = "//iframe")
    public WebElement iFrame;

    @Step
    public HotelStep runContactDetailsStepNextHotel(){
        anrede.click();
        titel.click();
        vorname.sendKeys("test");
        nachname.sendKeys("test");
        einheit.sendKeys("test");
        hubEinteilung.sendKeys("test");
        kostenstelle.sendKeys("test");
        strasseAndHouseNumber.sendKeys("test");
        plz.sendKeys("12345");
        ort.sendKeys("test");
        nativeLanguage.click();
        positionAtCompany.sendKeys("test");
        nextStepButton.click();
        try {
            new WebDriverWait(driver,10).until(ExpectedConditions
                    .presenceOfElementLocated(By.xpath("//input[@class='ng-tns-c30-0 ui-inputtext ui-widget ui-state-default ui-corner-all ng-star-inserted']")));
        }catch (Exception ex){
            email.sendKeys(getRandomString()+"@gmail.com");
            language.sendKeys("Deutschland");
            nextStepButton.click();
        }
        return new HotelStep(driver);
    }

    @Step
    public AttendancePage runContactDetailsStepNextAttendance(){
        try{
            new WebDriverWait(driver,7)
                    .until(ExpectedConditions.visibilityOf(iFrame));
        }
        catch (Exception ex){
            anrede.click();
            titel.click();
            new Actions(driver).moveToElement(einheit).perform();
            einheit.sendKeys("test");
            hubEinteilung.sendKeys("test");
            kostenstelle.sendKeys("test");
            strasseAndHouseNumber.sendKeys("test");
            plz.sendKeys("12345");
            ort.sendKeys("test");
            nativeLanguage.click();
            positionAtCompany.sendKeys("test");
            nextStepButton.click();
            try {
                new WebDriverWait(driver,7).until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath("//h2[text()='R2 SALES & MARKETING']")));
            }catch (Exception ex1){
                email.sendKeys(getRandomString()+"@gmail.com");
                language.sendKeys("Deutschland");
                nextStepButton.click();
            }
            return new AttendancePage(driver);
        }
        return new AttendancePage(driver);
    }



    private String getRandomString(){
        String randomSymbols="";
        char c;
        Random r = new Random();
        for(int i=0;i<8;i++){
            c = (char)(r.nextInt(26) + 'a');
            randomSymbols = randomSymbols + c;
        }
        return randomSymbols;
    }

}
