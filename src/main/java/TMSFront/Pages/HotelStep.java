package TMSFront.Pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HotelStep extends BasePage {
    public HotelStep(WebDriver webDriver) {
        super(webDriver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//input[@id='description_']")
    public WebElement descriptionField;

    @FindBy(xpath = "//label[@class='form-check-label']")
    private WebElement roomChooser;

    @FindBy(xpath = "//button[@class='button btn'][2]")
    private WebElement nextStepButton;

    @FindBy(xpath = "//div[@class='row']/div/button")
    private WebElement dontNeedTheHotel;

    @Step
    public OverviewPage runHotelStepWithHotel(){
        roomChooser.click();
        nextStepButton.click();
        return new OverviewPage(driver);
    }

    @Step
    public OverviewPage runHotelStepWithoutHotel(){
        new Actions(driver).moveToElement(dontNeedTheHotel).perform();
        dontNeedTheHotel.click();
        return new OverviewPage(driver);
    }
}
