package TMSFront.Tests;

import TMSFront.Pages.HomePage;
import TMSFront.Pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {
    @Test
    public void loginTest(){
        HomePage homePage = new LoginPage(driver)
                .loginToTMSFrontByUsualLogin();
        Assert.assertTrue(homePage.registrationButton.isDisplayed());
        Assert.assertTrue(homePage.logoJD.isDisplayed());
    }
}
